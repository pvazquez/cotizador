import React, { useState } from 'react';
import Error from './Error';
import styled from '@emotion/styled';


const Campo = styled.div`
    display: flex;
    margin-bottom: 1rem;
    align-items: center;
`;

const Label = styled.label`
    flex: 0 0 100px;
`;

const Select = styled.select`
    display: block;
    width: 100%;
    padding: 1rem;
    border: 1px solid #e1e1e1;
    -webkit-appearance: none;
`;

const InputRadio = styled.input`
    margin: 0 1rem;
`;

const Button = styled.button`
    background-color: #00838F;
    font-size: 16px;
    width: 100%;
    padding: 1rem;
    color: #fff;
    text-transform: uppercase;
    font-weight: bold;
    border: none;
    transition: background-color 3s ease;
    margin-top: 2rem;

    &:hover {
        cursor: pointer;
        background-color: #26C6DA;
    }
`;

const Formulario = ({cargarData}) => {

  const [valores_formulario, guardarValores ] = useState({
      marca: '',
      year: '',
      plan: ''
  })
  const [error, mostrarError] = useState(false);

  const {marca, year, plan} = valores_formulario;


  const obtenerDatosFormulario = (event) => {
      guardarValores({
          ...valores_formulario,
          //Esta sintaxis agrega al objeto nombre: valor. Ej marca: 'europeo'
          [event.target.name]: event.target.value
      })
  }

  const handleSubmit = (event) => {
      event.preventDefault();
      //Validacion
      if(marca === '' || year === '' || plan === '') {
          mostrarError(true);
          return;
      }

      mostrarError(false);

      //Mando valores del formulario al estado de App
      cargarData(valores_formulario);
  }



    return (
        <form onSubmit={handleSubmit}>

            {error ? <Error/> : null}
            <Campo>
                <Label>Marca</Label>
                  <Select
                      name="marca"
                      value={marca}
                      onChange={obtenerDatosFormulario}
                  >
                    <option value="">-- Seleccione --</option>
                    <option value="americano">Americano</option>
                    <option value="europeo">Europeo</option>
                    <option value="asiatico">Asiático</option>
                </Select>
            </Campo>

            <Campo>
                <Label>Año</Label>
                <Select
                    name="year"
                    value={year}
                    onChange={obtenerDatosFormulario}
                >
                    <option value="">-- Seleccione --</option>
                    <option value="2021">2021</option>
                    <option value="2020">2020</option>
                    <option value="2019">2019</option>
                    <option value="2018">2018</option>
                    <option value="2017">2017</option>
                    <option value="2016">2016</option>
                    <option value="2015">2015</option>
                    <option value="2014">2014</option>
                    <option value="2013">2013</option>
                    <option value="2012">2012</option>
                </Select>
            </Campo>

            <Campo>
                <Label>Plan</Label>
                <InputRadio
                    type="radio"
                    name="plan"
                    value="basico"
                    checked={plan === 'basico'}
                    onChange={obtenerDatosFormulario}
                />
                Básico
                <InputRadio
                    type="radio"
                    name="plan"
                    value="completo"
                    checked={plan === 'completo'}
                    onChange={obtenerDatosFormulario}
                />
                Completo
            </Campo>

            <Button type="submit">Cotizar</Button>

        </form>
    )
}

export default Formulario;
